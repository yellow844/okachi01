東京Node学園祭2016
===================

Node学園祭について
----------------------
公式サイト
http://nodefest.jp/2016/
日本最大のNode.jsのカンファレンス
今年は11月12日、13日に開催
12日はTutorialメイン
13日がセッションメイン
橋本が行ってきたのは13日目

Keynote I
------------------
* Douglas Crockford
http://www.crockford.com/
Boss of javaScript
JS Organizer
現在はPyPal
JavaScript: The Good Parts 著者

Keynote II
------------------
https://github.com/paypal/seifnode
SeifはPayPalで始まったオープンソースプロジェクトで、
Webを開発者やユーザーにとってより安全で簡単なアプリケーション配信システムに移行することを目標としています。


Node.js and Web Standards
-------------------
* James Snell
http://www.chmod777self.com/
IBM Tec lead
IBM's Technical Lead for Node.js
HTTP2がv8で入る予定
https://github.com/molnarg/node-http2

The journey toward ES modules.
-----------------------
* Bradley Farias
https://github.com/bmeck
TC39とNodeコミュニティのメンバー
主にES6モジュールとそれに纏わる話
Nodeで使えるのはv10
https://docs.google.com/presentation/d/1J9mN3N1KZXGwn7ufrv9UAIiUzKo8zsQaQwIKseebjzo/edit#slide=id.g18cb3b1c5f_0_263

React + Reduxを使った大規模商用サービスの開発
----------------------
* Naohiro Yoshida
株式会社リクルートテクノロジーズ所属
https://speakerdeck.com/yoshidan/nodefest2016
実用大規模商用サービスにおけるSSRの話などが非常に興味深い話です

リアルタイムデプロイサービスNow（仮）
----------------------
Naoyuki Kanezawa
Socket.ioチーム
Socket.ioチームが立ち上げたZeit所属
https://speakerdeck.com/nkzawa/introducing-now-and-next-dot-js
リアルタイムデプロイサービスNowとReactSSRフレームワークNext.jsの話

PostCSS: Build your own CSS processor
--------------------------------
* Masaaki Morishita
QiitaのIncrements所属
https://github.com/morishitter
Node.js製のCSS変換ツールであるPostCSSについての説明
https://speakerdeck.com/morishitter/postcss-build-your-own-css-processor

JavaScript による並列処理：共有メモリとロック
------------------------------
* Noritada Shimizu
Mozilla Japan エヴァンジェリスト
https://twitter.com/chikoski
https://speakerdeck.com/chikoski/20161113-nodefest
LowLevel-JavaScriptの話

LT:WebAssemblyに足りないもの
------------------------------
* 渋川よしき
http://blog.shibu.jp/
DeNA所属
Python SPA, Sphix-userなどPythonとも関わりの深く、
Node.jsでは日本のユーザーグループの初代代表（確か
https://docs.google.com/presentation/d/116KcjqmqlO9SnHt-G5cemWgWgepqLEvZJ1vSoptEjI4/edit