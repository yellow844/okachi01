# Nodo学園祭に行ってきた資料です
ただのリンク集ですが、もしよろしければご確認ください

Sphinxでbuild済みです
build/html/index.html
を参照ください

間違いなどございましたらご指摘お願いします

一応Sphixをmakeする場合

```
$ python3 -m venv python
$ source python/bin/activate
(python) pip install -r requirements.txt
(python) make html
```